# Build image
FROM golang:1.19 AS build-stage

WORKDIR /app
COPY . .

RUN CGO_ENABLED=0 go build -buildvcs=false -o /go/bin/nuntius ./cmd/

# Actual prod image
FROM alpine:latest
WORKDIR /app

COPY --from=build-stage /go/bin/nuntius /go/bin/nuntius

CMD [ "/go/bin/nuntius" ]