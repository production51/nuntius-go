package main

import (
	"fmt"
	"log"
	"net/http"
	"os"
	"production/marcbrun/nuntius/pkg/api"
	"production/marcbrun/nuntius/pkg/domain"
	"production/marcbrun/nuntius/pkg/storage"
	"production/marcbrun/nuntius/pkg/worker"
)

func main() {
	log.Println("Starting Nuntius...")

	// Channels
	// Worker -> Websocket
	newMessagesChan := make(chan domain.Message)

	// Components
	storage, err := storage.NewPostgresStorage()
	if err != nil {
		log.Fatalf("could not init storage: %v", err)
	}
	worker := worker.NewWorker(storage, newMessagesChan)
	websocketHandler := api.NewWebsocketHandler(worker)
	go websocketHandler.ForwardNewMessages(newMessagesChan)
	httpHandler := api.NewHttpHandler(worker)

	// Routes
	http.HandleFunc("/users", httpHandler.Users)
	http.HandleFunc("/messages", httpHandler.Messages)
	http.HandleFunc("/subscribe", websocketHandler.Subscribe)

	// Serve API
	port := os.Getenv("PORT")
	if port == "" {
		log.Fatal("PORT env variable is empty")
	}
	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%s", port), nil))
}
