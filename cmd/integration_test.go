package main

import (
	"encoding/json"
	"io"
	"net/http"
	"net/http/httptest"
	"production/marcbrun/nuntius/pkg/api"
	"production/marcbrun/nuntius/pkg/domain"
	"production/marcbrun/nuntius/pkg/storage"
	"production/marcbrun/nuntius/pkg/worker"
	"testing"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
)

func setUp(t *testing.T) *api.HttpHandler {
	// GIVEN
	// capacity 1000 is to avoid blocking the worker when it sends a message to the websocket handler
	newMessagesChan := make(chan domain.Message, 1000)
	storage := storage.NewTestPgStorage(t)
	worker := worker.NewWorker(storage, newMessagesChan)
	apiHandler := api.NewHttpHandler(worker)
	return apiHandler
}

func getResponse(t *testing.T, w *httptest.ResponseRecorder) api.Response {
	// THEN
	rawResp := w.Result()
	body, _ := io.ReadAll(rawResp.Body)
	resp := api.Response{}
	err := json.Unmarshal(body, &resp)
	assert.Nil(t, err)
	return resp
}

func TestUsersPost(t *testing.T) {
	apiHandler := setUp(t)

	resp := newUser(t, apiHandler, "alice")
	assert.Equal(t, http.StatusCreated, resp.Status)
	assert.Nil(t, resp.Data)
	assert.Equal(t, "", resp.Error)

	resp = newUser(t, apiHandler, "bob")
	assert.Equal(t, http.StatusCreated, resp.Status)
	assert.Nil(t, resp.Data)
	assert.Equal(t, "", resp.Error)

	resp = newUser(t, apiHandler, "alice")
	// fail since alice already exists
	assert.Equal(t, http.StatusInternalServerError, resp.Status)
	assert.NotEqual(t, "", resp.Error)
	assert.Nil(t, resp.Data)
}

func newUser(t *testing.T, apiHandler *api.HttpHandler, userName string) api.Response {
	// GIVEN
	w := httptest.NewRecorder()
	order := &domain.NewUserOrder{
		Name: userName,
	}
	req := order.ToHttpTestReq()

	// WHEN we create the user
	apiHandler.Users(w, req)

	// THEN
	return getResponse(t, w)
}

func TestMessagesPost(t *testing.T) {
	// GIVEN
	apiHandler := setUp(t)
	newUser(t, apiHandler, "alice")
	newUser(t, apiHandler, "bob")
	w := httptest.NewRecorder()
	order := &domain.NewMessageOrder{
		SenderName:   "alice",
		ReceiverName: "bob",
		Content:      "hello",
	}
	req := order.ToHttpTestReq()

	// WHEN alice sends a message to bob
	apiHandler.Messages(w, req)

	// THEN
	resp := getResponse(t, w)
	assert.Equal(t, http.StatusCreated, resp.Status)
	assert.Equal(t, "", resp.Error)
	assert.NotNil(t, resp.Data)
	assert.NotNil(t, resp.Data.Id)
	// created message ID must be a valid UUID
	_, err := uuid.Parse(resp.Data.Id)
	assert.Nil(t, err)
}

func TestMessagesGet(t *testing.T) {
	// GIVEN users alice and bob with a message
	apiHandler := setUp(t)
	newUser(t, apiHandler, "alice")
	newUser(t, apiHandler, "bob")
	newMessageOrder := &domain.NewMessageOrder{
		SenderName:   "alice",
		ReceiverName: "bob",
		Content:      "hello",
	}
	req := newMessageOrder.ToHttpTestReq()
	apiHandler.Messages(httptest.NewRecorder(), req)

	w := httptest.NewRecorder()
	req = httptest.NewRequest(http.MethodGet, "https://mydomain.com/messages?user=alice", nil)

	// WHEN alice fetches her messages
	apiHandler.Messages(w, req)

	// THEN
	resp := getResponse(t, w)
	assert.Equal(t, http.StatusOK, resp.Status)
	assert.Equal(t, "", resp.Error)
	assert.NotNil(t, resp.Data)
	assert.Equal(t, 1, len(resp.Data.Messages))
	assert.Equal(t, "alice", resp.Data.Messages[0].SenderName)
	assert.Equal(t, "bob", resp.Data.Messages[0].ReceiverName)
	assert.Equal(t, "hello", resp.Data.Messages[0].Content)
}

func TestMessagesPatch(t *testing.T) {
	// GIVEN users alice and bob with a message
	apiHandler := setUp(t)
	newUser(t, apiHandler, "alice")
	newUser(t, apiHandler, "bob")
	newMessageOrder := &domain.NewMessageOrder{
		SenderName:   "alice",
		ReceiverName: "bob",
		Content:      "hello",
	}
	newMessageReq := newMessageOrder.ToHttpTestReq()
	w := httptest.NewRecorder()
	apiHandler.Messages(w, newMessageReq)
	resp := getResponse(t, w)
	messageId, err := uuid.Parse(resp.Data.Id)
	assert.Nil(t, err)

	order := &domain.UpdateStatusOrder{
		MessageId: messageId,
		Update:    domain.Read,
	}
	req := order.ToHttpTestReq()
	w = httptest.NewRecorder()

	// WHEN bob reads the message
	apiHandler.Messages(w, req)

	// THEN
	resp = getResponse(t, w)
	assert.Equal(t, http.StatusOK, resp.Status)
	assert.Equal(t, "", resp.Error)
	assert.NotNil(t, resp.Data)
	assert.NotNil(t, resp.Data.Status)
	assert.False(t, resp.Data.Status.SentAt.IsZero())
	assert.False(t, resp.Data.Status.DeliveredAt.IsZero())
	assert.False(t, resp.Data.Status.ReadAt.IsZero())
}
