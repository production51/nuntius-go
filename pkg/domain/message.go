package domain

import (
	"github.com/google/uuid"
)

type Message struct {
	Id           uuid.UUID `json:"id"`
	SenderName   string    `json:"sender"`
	ReceiverName string    `json:"receiver"`
	Content      string    `json:"content"`
	Status
}
