package domain

import (
	"strings"
	"time"
)

type Status struct {
	SentAt      *time.Time `json:"sent_at"`
	DeliveredAt *time.Time `json:"delivered_at,omitempty"`
	ReadAt      *time.Time `json:"read_at,omitempty"`
}

type StatusUpdate string

const (
	Unknown   StatusUpdate = "unknown"
	Delivered StatusUpdate = "delivered"
	Read      StatusUpdate = "read"
)

var (
	statusUpdatesMap = map[string]StatusUpdate{
		"unknown":   Unknown,
		"delivered": Delivered,
		"read":      Read,
	}
)

func NewStatusUpdate(str string) (StatusUpdate, bool) {
	c, ok := statusUpdatesMap[strings.ToLower(str)]
	return c, ok
}
