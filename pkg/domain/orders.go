package domain

import (
	"net/http"
	"net/http/httptest"
	"net/url"
	"strings"

	"github.com/google/uuid"
)

type NewMessageOrder struct {
	SenderName   string `json:"sender"`
	ReceiverName string `json:"receiver"`
	Content      string `json:"content"`
}

func (r *NewMessageOrder) ToHttpTestReq() *http.Request {
	data := url.Values{}
	data.Set("sender", r.SenderName)
	data.Set("receiver", r.ReceiverName)
	data.Set("content", r.Content)

	req := httptest.NewRequest(http.MethodPost, "https://mydomain.com", strings.NewReader(data.Encode()))
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	return req
}

type GetMessagesOrder struct {
	UserName        string `json:"user"`
	OnlyUndelivered bool   `json:"only_undelivered"`
}

type SubscribeOrder struct {
	UserName string `json:"user"`
	FetchOld bool   `json:"fetch_old"`
}

type NewUserOrder struct {
	Name string
}

func (r *NewUserOrder) ToHttpTestReq() *http.Request {
	data := url.Values{}
	data.Set("name", r.Name)

	req := httptest.NewRequest(http.MethodPost, "https://mydomain.com", strings.NewReader(data.Encode()))
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	return req
}

type UpdateStatusOrder struct {
	MessageId uuid.UUID    `json:"message_id"`
	Update    StatusUpdate `json:"status"`
}

func (r *UpdateStatusOrder) ToHttpTestReq() *http.Request {
	data := url.Values{}
	data.Set("message_id", r.MessageId.String())
	data.Set("status", string(r.Update))

	req := httptest.NewRequest(http.MethodPatch, "https://mydomain.com", strings.NewReader(data.Encode()))
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	return req
}
