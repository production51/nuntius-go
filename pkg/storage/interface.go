package storage

import (
	"production/marcbrun/nuntius/pkg/domain"

	"github.com/google/uuid"
)

type Storage interface {
	NewUser(order *domain.NewUserOrder) error
	NewMessage(order *domain.NewMessageOrder) (uuid.UUID, error)
	UpdateMessageStatus(order *domain.UpdateStatusOrder) (*domain.Status, error)
	FetchMessage(messageId uuid.UUID) (*domain.Message, error)
	FetchMessages(order *domain.GetMessagesOrder) ([]domain.Message, error)
}
