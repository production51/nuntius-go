package storage

type Config struct {
	HoursToLive  int64
	StoreRetries int
}

var DefaultConfig = Config{
	HoursToLive:  24 * 30,
	StoreRetries: 5,
}
