package storage

import (
	"production/marcbrun/nuntius/pkg/domain"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestPgNewUser(t *testing.T) {
	// GIVEN
	s := NewTestPgStorage(t)
	defer s.cleanDatabase()
	// WHEN
	err := s.NewUser(&domain.NewUserOrder{Name: "alice"})
	// THEN
	assert.Nil(t, err)
	users, err := s.FetchUsers()
	assert.Nil(t, err)
	assert.Equal(t, 1, len(users))
	user, err := s.FetchUser("alice")
	assert.Nil(t, err)
	assert.NotNil(t, user)
	assert.Equal(t, "alice", user.Name)

	// Prevent registering same name twice
	// WHEN
	err = s.NewUser(&domain.NewUserOrder{Name: "alice"})
	// THEN
	assert.NotNil(t, err)
	users, err = s.FetchUsers()
	assert.Nil(t, err)
	assert.Equal(t, 1, len(users))
}

func TestPgNewMessage(t *testing.T) {
	// GIVEN an empty storage with no user
	s := NewTestPgStorage(t)
	defer s.cleanDatabase()
	// WHEN
	_, err := s.NewMessage(&domain.NewMessageOrder{
		SenderName:   "alice",
		ReceiverName: "bob",
		Content:      "hello",
	})
	// THEN
	// should fail if either user does not exist
	assert.NotNil(t, err)

	// GIVEN
	// Create sender and receiver and retry
	s.NewUser(&domain.NewUserOrder{Name: "alice"})
	s.NewUser(&domain.NewUserOrder{Name: "bob"})
	// WHEN
	_, err = s.NewMessage(&domain.NewMessageOrder{
		SenderName:   "alice",
		ReceiverName: "bob",
		Content:      "hello",
	})
	// THEN
	// should fail if either user does not exist
	assert.Nil(t, err)
	messages, err := s.FetchMessages(&domain.GetMessagesOrder{UserName: "alice"})
	assert.Nil(t, err)
	assert.Equal(t, 1, len(messages))
	msg := messages[0]
	expected := domain.Message{
		SenderName:   "alice",
		ReceiverName: "bob",
		Content:      "hello",
	}
	assert.Equal(t, expected.SenderName, msg.SenderName)
	assert.Equal(t, expected.ReceiverName, msg.ReceiverName)
	assert.Equal(t, expected.Content, msg.Content)
}

func TestPgFetchMessages(t *testing.T) {
	// GIVEN
	s := NewTestPgStorage(t)
	defer s.cleanDatabase()
	s.NewUser(&domain.NewUserOrder{Name: "alice"})
	s.NewUser(&domain.NewUserOrder{Name: "bob"})
	s.NewMessage(&domain.NewMessageOrder{
		SenderName:   "alice",
		ReceiverName: "bob",
		Content:      "hello",
	})
	s.NewMessage(&domain.NewMessageOrder{
		SenderName:   "alice",
		ReceiverName: "bob",
		Content:      "do you like this test?",
	})
	s.NewMessage(&domain.NewMessageOrder{
		SenderName:   "alice",
		ReceiverName: "bob",
		Content:      "I love this test",
	})
	s.NewMessage(&domain.NewMessageOrder{
		SenderName:   "alice",
		ReceiverName: "bob",
		Content:      "This test is the best",
	})
	// WHEN
	messagesAlice, err := s.FetchMessages(&domain.GetMessagesOrder{UserName: "alice"})
	// THEN
	assert.Nil(t, err)
	assert.Equal(t, 4, len(messagesAlice))
	// WHEN
	messagesBob, err := s.FetchMessages(&domain.GetMessagesOrder{UserName: "bob"})
	// THEN
	assert.Nil(t, err)
	assert.Equal(t, 4, len(messagesBob))

	assert.ElementsMatch(t, messagesAlice, messagesBob)
}

func TestPgUpdateMessageStatus(t *testing.T) {
	// GIVEN an empty storage with users and send message
	s := NewTestPgStorage(t)
	defer s.cleanDatabase()
	err := s.NewUser(&domain.NewUserOrder{Name: "alice"})
	assert.Nil(t, err)
	err = s.NewUser(&domain.NewUserOrder{Name: "bob"})
	assert.Nil(t, err)
	id, err := s.NewMessage(&domain.NewMessageOrder{
		SenderName:   "alice",
		ReceiverName: "bob",
		Content:      "hello",
	})
	assert.Nil(t, err)
	message, err := s.FetchMessage(id)
	assert.False(t, message.SentAt.IsZero())
	assert.True(t, message.DeliveredAt.IsZero())
	assert.True(t, message.ReadAt.IsZero())
	assert.Nil(t, err)
	// WHEN delivered
	status, err := s.UpdateMessageStatus(&domain.UpdateStatusOrder{
		MessageId: id,
		Update:    domain.Delivered,
	})
	// THEN
	assert.Nil(t, err)
	assert.False(t, status.SentAt.IsZero())
	assert.False(t, status.DeliveredAt.IsZero())
	assert.True(t, status.ReadAt.IsZero())
	message, err = s.FetchMessage(id)
	assert.Nil(t, err)
	assert.False(t, message.SentAt.IsZero())
	assert.False(t, message.DeliveredAt.IsZero())
	assert.True(t, message.ReadAt.IsZero())
	// WHEN read
	status, err = s.UpdateMessageStatus(&domain.UpdateStatusOrder{
		MessageId: id,
		Update:    domain.Read,
	})
	// THEN
	assert.Nil(t, err)
	assert.False(t, status.SentAt.IsZero())
	assert.False(t, status.DeliveredAt.IsZero())
	assert.False(t, status.ReadAt.IsZero())
	message, err = s.FetchMessage(id)
	assert.Nil(t, err)
	assert.False(t, message.SentAt.IsZero())
	assert.False(t, message.DeliveredAt.IsZero())
	assert.False(t, message.ReadAt.IsZero())
}
