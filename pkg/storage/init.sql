CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

CREATE TABLE IF NOT EXISTS users (
	name varchar(48) PRIMARY KEY
);

CREATE TABLE IF NOT EXISTS messages (
	id uuid PRIMARY KEY,
	sender_name varchar(48),
	receiver_name varchar(48),
	content varchar(512),
	sent_at TIMESTAMP,
	delivered_at TIMESTAMP,
	read_at TIMESTAMP,
	CONSTRAINT fk_sender
		FOREIGN KEY(sender_name) 
		REFERENCES users(name),
	CONSTRAINT fk_receiver
		FOREIGN KEY(receiver_name) 
		REFERENCES users(name)
);