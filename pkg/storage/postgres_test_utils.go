package storage

import (
	"fmt"
	"log"
	"os"
	"path/filepath"
	"runtime"
	"testing"

	"github.com/stretchr/testify/assert"
)

// Assumes a postgres container is already running e.g. using docker compose
func NewTestPgStorage(t *testing.T) *PostgreSqlStorage {
	postgresHost := os.Getenv("POSTGRES_HOST")
	os.Setenv("DATABASE_URL", fmt.Sprintf("postgres://postgres:postgres@%s?sslmode=disable", postgresHost))
	defer os.Unsetenv("DATABASE_URL")
	s, err := NewPostgresStorage()
	assert.Nil(t, err)
	err = s.setUpDatabase()
	assert.Nil(t, err)
	err = s.cleanDatabase()
	assert.Nil(t, err)
	return s
}

func (s *PostgreSqlStorage) setUpDatabase() error {
	log.Println("Set up database...")
	_, b, _, _ := runtime.Caller(0)
	basepath := filepath.Dir(b)
	content, err := os.ReadFile(basepath + "/init.sql")
	if err != nil {
		return err
	}
	_, err = s.database.Exec(string(content))
	if err != nil {
		return err
	}
	return nil
}

func (s *PostgreSqlStorage) cleanDatabase() error {
	log.Println("Clean database...")
	_, err := s.database.Exec("TRUNCATE TABLE messages, users;")
	if err != nil {
		return err
	}
	return nil
}
