package storage

import (
	"database/sql"
	"fmt"
	"log"
	"os"
	"production/marcbrun/nuntius/pkg/domain"
	"time"

	"github.com/avast/retry-go"
	"github.com/google/uuid"
	_ "github.com/lib/pq"
)

type PostgreSqlStorage struct {
	database *sql.DB
}

func NewPostgresStorage() (*PostgreSqlStorage, error) {
	databaseUrl := os.Getenv("DATABASE_URL")
	if databaseUrl == "" {
		return nil, fmt.Errorf("DATABASE_URL env variable is empty")
	}
	db, err := sql.Open("postgres", databaseUrl)
	if err != nil {
		return nil, fmt.Errorf("could not open connection to postgres database: %w", err)
	}
	err = retry.Do(
		func() error {
			// sql.Open does not guarantee trying to connect to the database. Ping will raise connection failures.
			err = db.Ping()
			if err != nil {
				log.Printf("ping failed on postgres database: %v, retrying...\n", err)
				return err
			}

			return nil
		},
	)
	if err != nil {
		return nil, fmt.Errorf("ping failed several times on postgres database: %w", err)
	}
	log.Println("ping succeeded, storage ready")

	return &PostgreSqlStorage{
		database: db,
	}, nil
}

func (s *PostgreSqlStorage) FetchUser(name string) (*domain.User, error) {
	user := domain.User{}
	err := s.database.QueryRow("SELECT name FROM users WHERE name = $1;", name).Scan(&user.Name)
	if err != nil {
		return nil, err
	}
	return &user, nil
}

func (s *PostgreSqlStorage) FetchUsers() ([]domain.User, error) {
	rows, err := s.database.Query("SELECT name FROM users;")
	if err != nil {
		return []domain.User{}, err
	}
	defer rows.Close()
	users := []domain.User{}
	for rows.Next() {
		user := domain.User{}
		rows.Scan(&user.Name)
		users = append(users, user)
	}
	return users, nil
}

func (s *PostgreSqlStorage) NewUser(order *domain.NewUserOrder) error {
	_, err := s.database.Exec(
		"INSERT INTO users(name) VALUES ($1);",
		order.Name,
	)
	return err
}

func (s *PostgreSqlStorage) NewMessage(order *domain.NewMessageOrder) (uuid.UUID, error) {
	id := uuid.New()
	_, err := s.database.Exec(
		"INSERT INTO messages(id, sender_name, receiver_name, content, sent_at, delivered_at, read_at) VALUES ($1, $2, $3, $4, $5, $6, $7);",
		id,
		order.SenderName,
		order.ReceiverName,
		order.Content,
		time.Now(),
		nil,
		nil,
	)
	return id, err
}

func (s *PostgreSqlStorage) UpdateMessageStatus(order *domain.UpdateStatusOrder) (*domain.Status, error) {
	msg, err := s.FetchMessage(order.MessageId)
	if err != nil {
		// message does not exist: cannot update
		return nil, err
	}
	var query string
	switch order.Update {
	case domain.Delivered:
		if msg.DeliveredAt != nil {
			// message was already delivered: do not update
			return &msg.Status, nil
		}
		query = "UPDATE messages SET delivered_at = $1 WHERE id = $2 RETURNING sent_at, delivered_at, read_at;"
	case domain.Read:
		if msg.ReadAt != nil {
			// message was already read: do not update
			return &msg.Status, nil
		}
		if msg.DeliveredAt != nil {
			query = "UPDATE messages SET read_at = $1 WHERE id = $2 RETURNING sent_at, delivered_at, read_at;"
		} else {
			query = "UPDATE messages SET read_at = $1, delivered_at = $1 WHERE id = $2 RETURNING sent_at, delivered_at, read_at;"
		}
	default:
		return nil, fmt.Errorf("unsupported statusUpdate value: %v", order.Update)
	}
	status := domain.Status{}
	err = s.database.QueryRow(
		query,
		time.Now(),
		order.MessageId,
	).Scan(&status.SentAt, &status.DeliveredAt, &status.ReadAt)
	return &status, err
}

func (s *PostgreSqlStorage) FetchMessage(messageId uuid.UUID) (*domain.Message, error) {
	msg := domain.Message{}
	err := s.database.QueryRow(
		"SELECT id, sender_name, receiver_name, content, sent_at, delivered_at, read_at FROM messages WHERE id = $1;",
		messageId,
	).Scan(&msg.Id, &msg.SenderName, &msg.ReceiverName, &msg.Content, &msg.SentAt, &msg.DeliveredAt, &msg.ReadAt)
	return &msg, err
}

func (s *PostgreSqlStorage) FetchMessages(order *domain.GetMessagesOrder) ([]domain.Message, error) {
	var rows *sql.Rows
	var err error
	if order.OnlyUndelivered {
		rows, err = s.database.Query(
			"SELECT id, sender_name, receiver_name, content, sent_at, delivered_at, read_at FROM messages WHERE receiver_name = $1 AND delivered_at IS NULL;",
			order.UserName,
		)
	} else {
		rows, err = s.database.Query(
			"SELECT id, sender_name, receiver_name, content, sent_at, delivered_at, read_at FROM messages WHERE sender_name = $1 OR receiver_name = $1;",
			order.UserName,
		)
	}
	if err != nil {
		return []domain.Message{}, err
	}
	defer rows.Close()
	messages := []domain.Message{}
	for rows.Next() {
		msg := domain.Message{}
		rows.Scan(&msg.Id, &msg.SenderName, &msg.ReceiverName, &msg.Content, &msg.SentAt, &msg.DeliveredAt, &msg.ReadAt)
		messages = append(messages, msg)
	}
	return messages, nil
}
