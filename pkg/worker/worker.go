package worker

import (
	"fmt"
	"production/marcbrun/nuntius/pkg/domain"
	"production/marcbrun/nuntius/pkg/storage"

	"github.com/google/uuid"
)

type Worker struct {
	storage  storage.Storage
	messages chan<- domain.Message
}

func NewWorker(storage storage.Storage, messages chan<- domain.Message) *Worker {
	return &Worker{
		storage:  storage,
		messages: messages,
	}
}

func (w *Worker) NewUser(order *domain.NewUserOrder) error {
	return w.storage.NewUser(order)
}

func (w *Worker) NewMessage(order *domain.NewMessageOrder) (uuid.UUID, error) {
	id, err := w.storage.NewMessage(order)
	if err != nil {
		return id, fmt.Errorf("could not create new message: %w", err)
	}
	err = w.sendToWebsocketHandler(id)
	if err != nil {
		return id, err
	}
	return id, nil
}

func (w *Worker) UpdateMessageStatus(order *domain.UpdateStatusOrder) (*domain.Status, error) {
	status, err := w.storage.UpdateMessageStatus(order)
	if err != nil {
		return nil, fmt.Errorf("could not update message status: %w", err)
	}
	err = w.sendToWebsocketHandler(order.MessageId)
	if err != nil {
		return status, err
	}
	return status, nil
}

func (w *Worker) FetchMessages(order *domain.GetMessagesOrder) ([]domain.Message, error) {
	return w.storage.FetchMessages(order)
}

func (w *Worker) sendToWebsocketHandler(messageId uuid.UUID) error {
	message, err := w.storage.FetchMessage(messageId)
	if err != nil {
		return fmt.Errorf("could not fetch newly created message: %w", err)
	}
	w.messages <- *message
	return nil
}
