package api

import (
	"net/http"
	"net/http/httptest"
	"production/marcbrun/nuntius/pkg/domain"
	"testing"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
)

func TestValidateUsersPost(t *testing.T) {
	// GIVEN
	expectedOrder := &domain.NewUserOrder{
		Name: "alice",
	}
	req := expectedOrder.ToHttpTestReq()

	// WHEN
	actualOrder, err := ValidateUsersPost(req)

	// THEN
	assert.Nil(t, err)
	assert.Equal(t, expectedOrder, actualOrder)
}

func TestValidateMessagesGet(t *testing.T) {
	// GIVEN
	req := httptest.NewRequest(http.MethodGet, "https://mydomain.com/messages?user=alice", nil)

	// WHEN
	order, err := ValidateMessagesGet(req)

	// THEN
	assert.Nil(t, err)
	assert.Equal(t, "alice", order.UserName)
}

func TestValidateMessagesPost(t *testing.T) {
	// GIVEN
	expectedOrder := &domain.NewMessageOrder{
		SenderName:   "alice",
		ReceiverName: "bob",
		Content:      "hello",
	}
	req := expectedOrder.ToHttpTestReq()

	// WHEN
	actualOrder, err := ValidateMessagesPost(req)

	// THEN
	assert.Nil(t, err)
	assert.Equal(t, expectedOrder, actualOrder)
}

func TestValidateMessagesPatch(t *testing.T) {
	// GIVEN
	expectedOrder := &domain.UpdateStatusOrder{
		MessageId: uuid.New(),
		Update:    domain.Read,
	}
	req := expectedOrder.ToHttpTestReq()

	// WHEN
	actualOrder, err := ValidateMessagesPatch(req)

	// THEN
	assert.Nil(t, err)
	assert.Equal(t, expectedOrder, actualOrder)
}
