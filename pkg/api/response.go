package api

import (
	"encoding/json"
	"log"
	"net/http"
	"production/marcbrun/nuntius/pkg/domain"
)

type Response struct {
	Status int           `json:"status"`
	Data   *ResponseData `json:"data,omitempty"`
	Error  string        `json:"error,omitempty"`
}

type ResponseData struct {
	Id       string           `json:"id"`
	Messages []domain.Message `json:"messages"`
	Status   *domain.Status   `json:"status,omitempty"`
}

func (r *Response) Write(w http.ResponseWriter) {
	msg, err := json.Marshal(r)
	if err != nil {
		log.Panic(err)
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("error upon marshalling response"))
	}
	w.WriteHeader(r.Status)
	w.Write(msg)
}
