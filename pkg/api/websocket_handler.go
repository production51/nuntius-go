package api

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"time"

	"nhooyr.io/websocket"

	"production/marcbrun/nuntius/pkg/domain"
	"production/marcbrun/nuntius/pkg/worker"
)

type subscription struct {
	username   string
	connection *websocket.Conn
	messages   chan domain.Message
}

type WebsocketHandler struct {
	worker        *worker.Worker
	subscriptions map[*subscription]bool
}

func NewWebsocketHandler(worker *worker.Worker) *WebsocketHandler {
	return &WebsocketHandler{
		worker:        worker,
		subscriptions: make(map[*subscription]bool),
	}
}

// Call as a goroutine
func (h *WebsocketHandler) ForwardNewMessages(messages <-chan domain.Message) {
	for message := range messages {
		for subscription := range h.subscriptions {
			if subscription.username == message.ReceiverName || subscription.username == message.SenderName {
				subscription.messages <- message
			}
		}
	}
}

func (h *WebsocketHandler) Subscribe(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	var resp *Response

	switch r.Method {
	case http.MethodGet:
		// Validate request
		subscribeOrder, err := ValidateSubscribe(r)
		if err != nil {
			resp = &Response{
				Status: http.StatusBadRequest,
				Error:  err.Error(),
			}
			break
		}
		err = h.acceptSubscription(subscribeOrder, w, r)
		log.Println(err)
	default:
		resp = &Response{
			Status: http.StatusBadRequest,
			Error:  "Unsupported Method",
		}
	}
	if resp != nil {
		resp.Write(w)
	}
}

func (h *WebsocketHandler) acceptSubscription(
	subscribeOrder *domain.SubscribeOrder,
	w http.ResponseWriter,
	r *http.Request,
) error {
	// Accept handshake
	c, err := websocket.Accept(w, r, &websocket.AcceptOptions{
		// TODO: only allow frontend, not all
		InsecureSkipVerify: true,
	})
	if err != nil {
		return err
	}
	sub := &subscription{
		username:   subscribeOrder.UserName,
		connection: c,
		messages:   make(chan domain.Message),
	}
	// Append to existing subscriptions
	h.subscriptions[sub] = true

	if subscribeOrder.FetchOld {
		// Send old messages
		oldMessages, err := h.worker.FetchMessages(&domain.GetMessagesOrder{
			UserName: subscribeOrder.UserName,
		})
		if err != nil {
			c.Close(websocket.StatusInternalError, "could not fetch old messages")
			return err
		}
		for _, message := range oldMessages {
			err = h.sendMessage(sub, message)
			if err != nil {
				c.Close(websocket.StatusInternalError, "could not send old message")
				return err
			}
		}
	}

	ctx, cancel := context.WithTimeout(context.Background(), time.Minute)
	defer cancel()
	for {
		select {
		case message := <-sub.messages:
			err = h.sendMessage(sub, message)
			if err != nil {
				h.closeSubscription(sub, websocket.StatusInternalError, "could not send new message")
				return err
			}
		case <-ctx.Done():
			h.closeSubscription(sub, websocket.StatusNormalClosure, "normal time out, retry connection")
			return ctx.Err()
		}
	}
}

// Marshals and sends a message to all subscriptions linked to the message's receiver
func (h *WebsocketHandler) sendMessage(sub *subscription, message domain.Message) error {
	// Marshal
	messageBytes, err := json.Marshal(message)
	if err != nil {
		return fmt.Errorf("could not marshal message: %w", err)
	}
	// Send
	if sub.username != message.ReceiverName && sub.username != message.SenderName {
		return fmt.Errorf("subscription user is neither sender nor receiver of this message")
	}
	err = h.writeWithTimeout(sub, messageBytes)
	if err != nil {
		return fmt.Errorf("could not send message to %s: %w", sub.username, err)
	}

	return nil
}

func (h *WebsocketHandler) writeWithTimeout(sub *subscription, message []byte) error {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	err := sub.connection.Write(ctx, websocket.MessageText, message)
	if err != nil {
		return fmt.Errorf("could not write message (%w)", err)
	}
	return nil
}

func (h *WebsocketHandler) closeSubscription(sub *subscription, code websocket.StatusCode, reason string) {
	close(sub.messages)
	sub.connection.Close(code, reason)
	delete(h.subscriptions, sub)
}
