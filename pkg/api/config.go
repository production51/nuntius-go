package api

type Config struct {
	Host string
	Port int
}

var DefaultConfig = Config{
	Host: "localhost",
	Port: 8080,
}
