package api

import (
	"log"
	"net/http"
	"production/marcbrun/nuntius/pkg/worker"
)

type HttpHandler struct {
	worker *worker.Worker
}

func NewHttpHandler(worker *worker.Worker) *HttpHandler {
	return &HttpHandler{
		worker: worker,
	}
}

func (h *HttpHandler) Users(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	var resp Response
	switch r.Method {
	case http.MethodPost:
		resp = h.usersPost(r)
	default:
		resp = Response{
			Status: http.StatusBadRequest,
			Error:  "Unsupported Method",
		}
	}
	resp.Write(w)
}

func (h *HttpHandler) usersPost(r *http.Request) Response {
	newUserOrder, err := ValidateUsersPost(r)
	if err != nil {
		return Response{
			Status: http.StatusBadRequest,
			Error:  err.Error(),
		}
	}
	err = h.worker.NewUser(newUserOrder)
	if err != nil {
		log.Printf("could not create new user: %v", err)
		// TODO: distinguish internal errors from "user already exists"
		return Response{
			Status: http.StatusInternalServerError,
			Error:  "could not create new user",
		}
	}
	return Response{
		Status: http.StatusCreated,
	}
}

func (h *HttpHandler) Messages(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	var resp Response
	switch r.Method {
	case http.MethodGet:
		resp = h.messagesGet(r)
	case http.MethodPost:
		resp = h.messagesPost(r)
	case http.MethodPatch:
		resp = h.messagesPatch(r)
	default:
		resp = Response{
			Status: http.StatusBadRequest,
			Error:  "Unsupported Method",
		}
	}
	resp.Write(w)
}

func (h *HttpHandler) messagesGet(r *http.Request) Response {
	order, err := ValidateMessagesGet(r)
	if err != nil {
		return Response{
			Status: http.StatusBadRequest,
			Error:  err.Error(),
		}
	}
	messages, err := h.worker.FetchMessages(order)
	if err != nil {
		log.Printf("could not fetch messages: %v", err)
		return Response{
			Status: http.StatusInternalServerError,
			Error:  "could not fetch messages",
		}
	}
	return Response{
		Status: http.StatusOK,
		Data: &ResponseData{
			Messages: messages,
		},
	}
}

func (h *HttpHandler) messagesPost(r *http.Request) Response {
	newMessageOrder, err := ValidateMessagesPost(r)
	if err != nil {
		return Response{
			Status: http.StatusBadRequest,
			Error:  err.Error(),
		}
	}
	messageId, err := h.worker.NewMessage(newMessageOrder)
	if err != nil {
		log.Printf("could not create new message: %v", err)
		return Response{
			Status: http.StatusInternalServerError,
			Error:  "could not create new message",
		}
	}
	return Response{
		Status: http.StatusCreated,
		Data: &ResponseData{
			Id: messageId.String(),
		},
	}
}

func (h *HttpHandler) messagesPatch(r *http.Request) Response {
	order, err := ValidateMessagesPatch(r)
	if err != nil {
		return Response{
			Status: http.StatusBadRequest,
			Error:  err.Error(),
		}
	}
	status, err := h.worker.UpdateMessageStatus(order)
	if err != nil {
		log.Printf("could not update message status: %v", err)
		return Response{
			Status: http.StatusInternalServerError,
			Error:  "could not update message status",
		}
	}
	return Response{
		Status: http.StatusOK,
		Data: &ResponseData{
			Status: status,
		},
	}
}
