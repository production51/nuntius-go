package api

import (
	"fmt"
	"net/http"
	"production/marcbrun/nuntius/pkg/domain"

	"github.com/google/uuid"
)

func ValidateUsersPost(r *http.Request) (*domain.NewUserOrder, error) {
	err := r.ParseForm()
	if err != nil {
		return nil, fmt.Errorf("could not parse POST body: %w", err)
	}
	userName := r.PostFormValue("name")
	if userName == "" {
		return nil, fmt.Errorf("missing or empty parameter: name")
	}
	return &domain.NewUserOrder{
		Name: userName,
	}, nil
}

func ValidateMessagesGet(r *http.Request) (*domain.GetMessagesOrder, error) {
	userName := r.URL.Query().Get("user")
	if userName == "" {
		return nil, fmt.Errorf("missing or empty parameter: user")
	}
	return &domain.GetMessagesOrder{
		UserName: userName,
	}, nil
}

func ValidateSubscribe(r *http.Request) (*domain.SubscribeOrder, error) {
	userName := r.URL.Query().Get("user")
	if userName == "" {
		return nil, fmt.Errorf("missing or empty parameter: user")
	}
	fetchOldStr := r.URL.Query().Get("fetch_old")
	if fetchOldStr == "" {
		return nil, fmt.Errorf("missing or empty parameter: fetch_old")
	}
	return &domain.SubscribeOrder{
		UserName: userName,
		FetchOld: fetchOldStr == "true",
	}, nil
}

func ValidateMessagesPost(r *http.Request) (*domain.NewMessageOrder, error) {
	err := r.ParseForm()
	if err != nil {
		return nil, fmt.Errorf("could not parse POST body: %w", err)
	}
	senderName := r.PostFormValue("sender")
	if senderName == "" {
		return nil, fmt.Errorf("missing or empty parameter: sender")
	}
	receiverName := r.PostFormValue("receiver")
	if senderName == "" {
		return nil, fmt.Errorf("missing or empty parameter: receiver")
	}
	content := r.PostFormValue("content")
	if senderName == "" {
		return nil, fmt.Errorf("missing or empty parameter: content")
	}
	return &domain.NewMessageOrder{
		SenderName:   senderName,
		ReceiverName: receiverName,
		Content:      content,
	}, nil
}

func ValidateMessagesPatch(r *http.Request) (*domain.UpdateStatusOrder, error) {
	err := r.ParseForm()
	if err != nil {
		return nil, fmt.Errorf("could not parse PATCH body: %w", err)
	}
	messageId, err := uuid.Parse(r.PostFormValue("message_id"))
	if err != nil {
		return nil, fmt.Errorf("parameter `message_id` must be a valid UUID string")
	}
	statusUpdate, ok := domain.NewStatusUpdate(r.PostFormValue("status"))
	if !ok {
		return nil, fmt.Errorf("accepted values for parameter `status` are 'delivered' and 'read'")
	}
	return &domain.UpdateStatusOrder{
		MessageId: messageId,
		Update:    statusUpdate,
	}, nil
}
