# Nuntius

Simple messenger API with features:
- log user in
- list user conversations
- list conversation messages
- send message to other user
- update message sent/deliver/read status
