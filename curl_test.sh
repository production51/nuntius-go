echo "Create user alice"
curl -s -d "name=alice" -X POST localhost:8080/users
echo
echo "Create user bob"
curl -s -d "name=bob" -X POST localhost:8080/users
echo
echo "Send hello message from alice to bob"
MESSAGE_ID=`curl -s -d "sender=alice&receiver=bob&content=hello" -X POST localhost:8080/messages | jq -r '.data.id'`
echo
echo "Fetch messages as bob"
curl -s 'localhost:8080/messages?user=bob' | jq
echo
echo "Mark message as delivered"
curl -s -d "message_id=$MESSAGE_ID&status=delivered" -X PATCH localhost:8080/messages
echo
echo "Mark message as read"
curl -s -d "message_id=$MESSAGE_ID&status=read" -X PATCH localhost:8080/messages
